//
//  KSPrint.h
//  OCLint
//
//  Created by MemetGhini on 2019/5/6.
//  Copyright © 2019 memetghini. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSPrint : NSObject

- (void)doIT;

@end

NS_ASSUME_NONNULL_END
