echo "正在安装 OCLint ..."
brew tap oclint/formulae
brew install oclint

if [ which jq &> /dev/null ]
then
	echo "正在安装 jq ..."
	brew install jq
else
	echo "jq 已被安装!!!"
fi