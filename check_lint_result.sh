#!/bin/bash
numberOfFilesWithViolations=$(cat oclint_result.json | jq '.summary.numberOfFilesWithViolations')
echo "OCLint 有警告的文件数量为：$numberOfFilesWithViolations ！！！"
if [ $numberOfFilesWithViolations -gt 0 ] 
then
	echo "OCLint 不合法文件数量大于0，请检查$(pwd)/OCLintScript//oclint_result.json"
	exit 1
else
	echo "OCLint 没有出现任何警告警告，检测通过"
	exit 0
fi